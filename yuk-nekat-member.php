<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.yukdiorder.com
 * @since             1.0.0
 * @package           Yuk_Nekat_Member
 *
 * @wordpress-plugin
 * Plugin Name:       Yuk Nekat Member
 * Plugin URI:        http://www.yukdiorder.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Yuk Diorder Team
 * Author URI:        http://www.yukdiorder.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       yuk-nekat-member
 * Domain Path:       /languages
 */


if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'PLUGIN_NAME_VERSION', '1.0.0' );

function activate_yuk_nekat_member() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-yuk-nekat-member-activator.php';
	Yuk_Nekat_Member_Activator::activate();
}

function deactivate_yuk_nekat_member() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-yuk-nekat-member-deactivator.php';
	Yuk_Nekat_Member_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_yuk_nekat_member' );
register_deactivation_hook( __FILE__, 'deactivate_yuk_nekat_member' );


require plugin_dir_path( __FILE__ ) . 'includes/class-yuk-nekat-member.php';

function run_yuk_nekat_member() {

	$plugin = new Yuk_Nekat_Member();
	$plugin->run();

}

run_yuk_nekat_member();

?>