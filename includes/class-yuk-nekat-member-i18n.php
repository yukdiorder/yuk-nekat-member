<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.yukdiorder.com
 * @since      1.0.0
 *
 * @package    Yuk_Nekat_Member
 * @subpackage Yuk_Nekat_Member/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Yuk_Nekat_Member
 * @subpackage Yuk_Nekat_Member/includes
 * @author     Yuk Diorder Team <nain.client@gmail.com>
 */
class Yuk_Nekat_Member_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'yuk-nekat-member',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
