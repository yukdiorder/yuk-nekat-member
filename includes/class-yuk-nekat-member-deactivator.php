<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.yukdiorder.com
 * @since      1.0.0
 *
 * @package    Yuk_Nekat_Member
 * @subpackage Yuk_Nekat_Member/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Yuk_Nekat_Member
 * @subpackage Yuk_Nekat_Member/includes
 * @author     Yuk Diorder Team <nain.client@gmail.com>
 */
class Yuk_Nekat_Member_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
