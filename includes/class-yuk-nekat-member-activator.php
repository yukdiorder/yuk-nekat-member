<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.yukdiorder.com
 * @since      1.0.0
 *
 * @package    Yuk_Nekat_Member
 * @subpackage Yuk_Nekat_Member/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Yuk_Nekat_Member
 * @subpackage Yuk_Nekat_Member/includes
 * @author     Yuk Diorder Team <nain.client@gmail.com>
 */
class Yuk_Nekat_Member_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
