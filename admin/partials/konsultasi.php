
<div class="container-fluid" style="padding:20px;">
  
    <?php
        $args = array(
            'p'                      => '5729',
        );
        $query = new WP_Query( $args );
        while ( $query->have_posts() ) : $query->the_post();
    ?>

<div class="row">
    <div class="col-sm-4">
      <h3><?= the_title(); ?></h3>
      <article>
	  <?= the_content(); ?>
	  </article>
      <?php endwhile;  ?>
      <?php comment_form(); ?>
      
    </div>
    <div class="col-sm-4">
    <h3>Tanya Jawab</h3>
    <ol class="commentlist">
	<?php
		//Gather comments for a specific page/post 
		$comments = get_comments(array(
			'post_id' => '5729',
			'status' => 'approve' //Change this to the type of comments to be displayed
		));

		//Display the list of comments
		wp_list_comments(array(
			'per_page' => 10, //Allow comment pagination
			'reverse_top_level' => false //Show the oldest comments at the top of the list
		), $comments);
	?>
    </ol>
    </div>
  
</div>


