<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.yukdiorder.com
 * @since      1.0.0
 *
 * @package    Yuk_Nekat_Member
 * @subpackage Yuk_Nekat_Member/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="container">
    <div class="row">
        <div class="col-lg-12 my-3">
            <div class="pull-right">
                <div class="btn-group">
                    <button class="btn btn-info" id="list">
                        List View
                    </button>
                    <button class="btn btn-danger" id="grid">
                        Grid View
                    </button>
                </div>
            </div>
        </div>
    </div> 
    
    <div id="products" class="row view-group">
    <?php 
    // WP_Query arguments
    $args = array(
        'post_type'              => array( 'lesson' ),
        'post_status'            => array( 'published' ),
    );

        $lesson = new WP_Query( $args );

        // The Loop
        if ( $lesson->have_posts() ) {
            while ( $lesson->have_posts() ) {
                $lesson->the_post();
                include "lesson.php";
                // do something
            }
        } else {
            // no posts found
        }

        // Restore original Post Data
        wp_reset_postdata();
    ?>
</div>