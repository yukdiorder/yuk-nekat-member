<div class="item col-xs-4 col-lg-4">
                    <div class="thumbnail card">
                        <div class="img-event">
                            <img class="group list-group-image img-fluid" src="http://localhost/wplab/wp-content/uploads/2018/11/cover-segmentasi.jpg" alt="" />
                        </div>
                        <div class="caption card-body">
                            <h4 class="group card-title inner list-group-item-heading">
                                <?= the_title(); ?></h4>
                            <p class="group inner list-group-item-text">
                                <?= the_content(); ?></p>
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p class="lead">Tingkat : Dasar </p>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <a class="btn btn-success" href="<?= the_permalink(); ?>">Pelajari</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>