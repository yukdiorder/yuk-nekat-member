<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.yukdiorder.com
 * @since      1.0.0
 *
 * @package    Yuk_Nekat_Member
 * @subpackage Yuk_Nekat_Member/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Yuk_Nekat_Member
 * @subpackage Yuk_Nekat_Member/admin
 * @author     Yuk Diorder Team <nain.client@gmail.com>
 */
class Yuk_Nekat_Member_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Yuk_Nekat_Member_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Yuk_Nekat_Member_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_register_style('bootstrap','https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css');
		
		wp_enqueue_style('bootstrap');
		//wp_register_style('yuk-style', plugin_dir_url( __FILE__ ) . 'css/yuk-style.css');
		//wp_enqueue_style('yuk-style');
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/yuk-nekat-member-admin.css');

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Yuk_Nekat_Member_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Yuk_Nekat_Member_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_register_script('js_bootstrap','https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js');
		wp_enqueue_script('js_bootstrap');
		//wp_register_script('yuk-script', plugin_dir_url( __FILE__ ) . 'js/yuk-script.js');
		//wp_enqueue_script('yuk-script');
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/yuk-nekat-member-admin.js');

	}
	public function admin_menu() {
		add_menu_page('Yuk Nekat Admin','Yuk Nekat','menu-nekat', 'yuk-nekat', array($this,'admin_options'),'',null );
		add_submenu_page( 'yuk-nekat', 'Materi', 'Overview','menu-nekat', 'overview-nekat', array($this,'admin_overview'));
		add_submenu_page( 'yuk-nekat', 'Konsultasi', 'Konsultasi','menu-nekat', 'konsultasi-nekat', array($this,'admin_konsultasi') );
	}
	
	public function admin_options() {
		include "/partials/options.php";
	}
	
	public function admin_overview() {
		include "/partials/overview.php";
	}

	public function admin_konsultasi(){
		include "/partials/konsultasi.php";
	}
	public function aktifkan_menu(){
		add_action('admin_menu','admin_menu');
	}

	public function tambah_role(){
		/* menambahkan role dan capability*/
		$role = new WP_Roles;
		$role->add_role('silvi', 'silvi member');
		$role->add_cap('silvi','menu-nekat');
		$role->add_cap('administrator','menu-nekat');
		$role->add_cap('s2member_level1', 'menu-nekat');
		$role->add_cap('s2member_level2', 'menu-nekat');
		$role->add_cap('s2member_level3', 'menu-nekat');
		$role->add_cap('s2member_level4', 'menu-nekat');
	}


}
